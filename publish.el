(require 'org)
;; Publishing
(require 'ox-html)
(require 'ox-publish)
;; Bibliography support
(require 'oc)
(require 'citeproc) ;; for HTML
(require 'oc-csl) ;; for HTML

;; Force publishing of unchanged files.
(setq org-publish-use-timestamps-flag nil)

;; Set up website favicon.
(setq org-html-head-extra "<link rel=\"icon\" type=\"image/x-icon\"
href=\"https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/resume/favicon.ico\"/>")

(setq org-cite-csl-styles-dir "./include")
(setq org-cite-export-processors
      '((t . (csl "ieee-with-url.csl"))))

;; Define publishing targets.
(setq org-publish-project-alist
      (list
       (list "resume"
             :base-directory "."
             :base-extension "org"
             :publishing-directory "./public"
             :publishing-function '(org-html-publish-to-html)
             :with-author t
             :with-email t
             :with-creator t
             :with-date t
             :section-numbers nil
             :with-toc t)))
