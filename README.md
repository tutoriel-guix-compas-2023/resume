# Tutoriel Guix - Compas 2023 : Résumé 

[![pipeline status](https://gitlab.inria.fr/tutoriel-guix-compas-2023/resume/badges/master/pipeline.svg)](https://gitlab.inria.fr/tutoriel-guix-compas-2023/resume/-/commits/master)

Ce répertoire contient le résumé du tutoriel Guix que l'on propose pour la
conférence Compas 2023.

[Consulter](https://tutoriel-guix-compas-2023.gitlabpages.inria.fr/resume)

## Instructions de construction

Pour exporter localement le texte du résumé dans `resume.org` en HTML, utilisez
la commande ci-dessous.

```bash
guix time-machine -C ./include/channels.scm -- shell --pure \
    -m ./include/manifest.scm -- emacs --batch --no-init-file \
    --load publish.el --funcall org-publish-all
```

Le résultat, c'est-à-dire le fichier `resume.html`, devrait alors apparaître
dans le dossier `public`.
